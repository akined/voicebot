'use strict'
const AWS = require('aws-sdk');


// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const dynamoTable = 'sf-conccen-rsch-podd-us1-claimdetails';
let response_dynamo;
const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(intent_name) {
    try {
        // Get the FAQ respnose for given intent_name.

        const intentResponse = {}
        const params = {
            Key: {
                'CustomerPhoneNumber':{S:intent_name}
            },
            TableName: dynamoTable
        };
        console.log(intentResponse, "intentresponse ")
        console.log(params)
       const dynamodbResponse = await dynamodb.getItem(params).promise();
         console.log(dynamodbResponse, "dyanmoresponse ")
        //const response = flattenDynamoDbResponse(dynamodbResponse);
      //console.log(response, "response ")
      //  intentResponse["Response"] = response['Item']['FAQResponse'];
        
        //intentResponse["RelatedQuestions"] = response['Item']['RelatedQuestions'];

        //console.log("The related questions are as follows: ", intentResponse["RelatedQuestions"]);
        console.log("The intent resopnse is :", intentResponse);

        // Finally return the composed intent response to elicitslot.
        return dynamodbResponse;
    } catch (err) {
        console.log('actual ::: '+err)
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        return err.response['Error']['Message'];
    }
}

// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //

const get_slots = (intent_request) => intent_request['currentIntent']['slots'];

const elicitSlot = (fulfillment_state, message) => {
    const dialogResponse = {
        'dialogAction': {
            //'outputDialogMode': 'Voice',
            'type': 'Close',
           'fulfillmentState': 'Fulfilled',
        //   'intentName': intent_request['currentIntent']['name'],
            'message': {
                'contentType': 'SSML',
                'content': message
            },
        // 'slots': intent_request['currentIntent']['slots'],
        //  'slotToElicit':Object.keys(intent_request['currentIntent']['slots'])[0],
        }
    }
    // Check if we are given a list of related questions for this intent.
  /* if (related_intents.length > 0) {
        // Generate the response card to be added to dialogResponse.
        const responseCard = {
            'version': 1,
            'contentType': 'application/vnd.amazonaws.card.generic',
            'genericAttachments': []
        }
        const questions = []
        // Create buttons for each of the realted intent question.
        related_intents.map((question) => {
            const question_button = {
                'text': question,
                'value': question
            };
            questions.push(question_button);
        });

        console.log("Question buttons are ", questions);
        // Create the generic attachment to add to the lex response.
        const attachment = {
            'title': 'You can also ask..',
            'buttons': questions
        };
        console.log("The generic attachment is : ", attachment);
        // Attach the attachments to the response card.
        responseCard['genericAttachments'].push(attachment)
        // Attach response card to dialogResponse.
        dialogResponse['dialogAction'] = responseCard
        console.log("The dialogResponse will be formatted as : ", dialogResponse);
    }*/
    return dialogResponse
}
const elicitSlot_NoSlot = (fulfillment_state, message, related_intents, intent_request) => {
    const dialogResponse = {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': 'Fulfilled',
           //'intentName': intent_request['currentIntent']['name'],
            'message': {
                'contentType': "SSML",
                'content': message
            },
            //'slots': intent_request['currentIntent']['slots'],
            //'slotToElicit':Object.keys(intent_request['currentIntent']['slots'])[0],
        }
    }
    return dialogResponse
}

const elicitSlot_getValue = (slotToElicit, message,intent_request) => {
    const dialogResponse = {
        'dialogAction': {
            'type': 'ElicitSlot',
          //'fulfillmentState': 'Fulfilled',
         // 'intentName': intent_request['currentIntent']['name'],
            'message': {
                "contentType": "PlainText",
                'content': message
            },
         'slots': intent_request['currentIntent']['slots'],
          'slotToElicit': slotToElicit,
        }
    }
    return dialogResponse
}

// ---------------------------------------------- Validation Helper Functions ---------------------------------------------- //

const build_validation_result = (is_valid, violated_slot, message_content) => {
    if (message_content) {
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }
    } else {
        return {
            'isValid': is_valid,
            'violatedSlot': violated_slot,
            'message': { 'contentType': 'CustomPayload', 'content': message_content }
        }
    }
};

async function CustomerPhoneNumber(intent_request) {

    // Provides instructions on how to generate a new document.

    //let speech = new SsmlOutputSpeech();
    const document_type = get_slots(intent_request)["Nums"]
   // const slotTwo = get_slots(intent_request)["slotTwo"]
    console.log({document_type})
    let CP = document_type;
    if(CP.length==10)
    CP='+1'+CP;
    // const slotTwo = get_slots(intent_request)["slotTwo"];
    // console.log({slotTwo})
   // console.log({slotTwo})
    //Perform basic validation on the supplied input slots.
        response_dynamo = await get_response(CP+'')
        const value = response_dynamo.Item['DateOfLoss']['S'].toString();
        const output = '<speak>Ok, I found a claim that happened on<say-as interpret-as="date">'+value+'</say-as> Is that what you are calling about ?</speak>'
        var message = output
        //return elicitSlot('Fulfilled', 'Ok, I found a claim that happened on  '+response_dynamo.Item['DateOfLoss']['S']+' Is that what you are calling about ?')
      return elicitSlot('Fulfilled', message );
      // console.log({el});
          // const slotTwo = get_slots(intent_request)["slotTwo"];
          //   console.log({slotTwo})
          //   return elicitSlot('Fulfilled', 'Ok, I found a claim that happened on  '+response_dynamo.Item['DateOfLoss']['S']+' Is that what you are calling about ?')
}

async function ConfIntent(intent_request) {

    // Provides instructions on how to generate a new document.
    //console.log('IIIIIIIIIIINNN '+response_dynamo);
       //return elicitSlot_NoSlot('Fulfilled', 'Here is your claim '+response_dynamo.Item['ClaimNumber']['S'], intent_request)
       //return elicitSlot_NoSlot('Fulfilled', 'Ok, How can i help you', intent_request)
      return elicitSlot_NoSlot('Fulfilled',  `<speak> 'Ok, How can i help you'</speak>`, ['err'],intent_request)
}
async function ConsnoIntent(intent_request) {

    // Provides instructions on how to generate a new document.
    console.log('IIIIIIIIIIINNN '+response_dynamo);
       //return elicitSlot_NoSlot('Fulfilled', 'Here is your claim '+response_dynamo.Item['ClaimNumber']['S'], intent_request)
       return elicitSlot_NoSlot('Fulfilled',`<speak> I am conneting you to rep</speak>`, ['err'], intent_request)
}



// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    //Called when the user specifies an intent for this bot.
    console.log(intent_request)

    const intent_name = intent_request['currentIntent']['name']
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'ConfIntent') {
        return await ConfIntent(intent_request);
    } else if (intent_name == 'CustomerPhoneNumber') {
        return await CustomerPhoneNumber(intent_request);
    }  else if (intent_name == 'ConsnoIntent') {
        return await ConsnoIntent(intent_request)
    } 
    else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return await elicitSlot('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
};

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
}


